-- auto-generated definition
create table user
(
    id           bigint auto_increment
        primary key,
    userAccount  varchar(256)                           null comment '账号',
    username     varchar(256)                           null comment '用户昵称',
    avatarUrl    varchar(1024)                          null comment '用户头像',
    gender       tinyint                                null comment '性别',
    userPassword varchar(512) default '1234566'         not null comment '用户密码',
    email        varchar(512)                           null comment '邮箱',
    userStatus   int          default 0                 null comment '状态 0-正常',
    phone        varchar(128)                           null comment '手机号',
    createTime   datetime     default CURRENT_TIMESTAMP null comment '创建时间',
    updateTime   datetime     default CURRENT_TIMESTAMP null comment '更新时间',
    isDelete     tinyint      default 0                 not null comment '是否删除',
    planetCode   varchar(512)                           null comment '星球编号',
    userRole     int          default 0                 not null comment '普通用户-0 管理员-1'
)
    comment '用户';