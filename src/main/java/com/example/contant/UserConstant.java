package com.example.contant;


/**
 * 用户登录
 *
 * @Author cao
 */
public interface UserConstant {
    /**
     * 用户登录态键
     */
    String USER_LOGIN_STATE  = "userLoginState";

     //------权限-------

    /**
     * 默认权限
     */
    int DEFAULT_ROLE = 0;
    /**
     * 管理员
     */
    int ADMIN_ROLE = 1;



}
