package com.example.mapper;

import com.example.model.domin.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 60500
* @description 针对表【user(用户)】的数据库操作Mapper
* @createDate 2023-10-12 22:51:48
* @Entity com.example.model.User
*/
public interface UserMapper extends BaseMapper<User> {

}




